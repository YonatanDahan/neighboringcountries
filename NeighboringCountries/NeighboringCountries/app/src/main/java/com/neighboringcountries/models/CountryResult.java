package com.neighboringcountries.models;

import java.util.ArrayList;
import java.util.List;



public class CountryResult {

    List<Country> countryList = new ArrayList<Country>();

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

}
