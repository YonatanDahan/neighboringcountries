package com.neighboringcountries;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.neighboringcountries.adapters.CountryAdapter;
import com.neighboringcountries.models.Country;
import com.neighboringcountries.rest.IRestCountries;
import com.neighboringcountries.rest.RestCountriesClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    private List<Country> countryList = new ArrayList<>();
    private RecyclerView countryRecyclerViewCb;
    private CountryAdapter countryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryRecyclerViewCb = (RecyclerView)findViewById(R.id.list_countries);
        getCountryInfo();
    }

    void getCountryInfo() {
        try {
            countryList = new ArrayList<Country>();

            IRestCountries iRestCountries = RestCountriesClient.getClient();

            Call<List<Country>> call = iRestCountries.getInfo();

            call.enqueue(new Callback<List<Country>>() {
                @Override
                public void onResponse(Response<List<Country>> response, Retrofit retrofit) {

                    try {
                        List<Country> result = response.body();
                        countryList = result;
                        RecyclerView.LayoutManager layoutManagerObj =
                                new LinearLayoutManager(getApplicationContext());
                        countryRecyclerViewCb.setLayoutManager(layoutManagerObj);
                        countryRecyclerViewCb.setItemAnimator(new DefaultItemAnimator());
                        countryRecyclerViewCb.addItemDecoration
                                (new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));
                        countryAdapter  = new CountryAdapter(countryList, MainActivity.this);
                        countryRecyclerViewCb.setAdapter(countryAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    t.getMessage();

                }
            });
        }
        catch (Exception Ex) {
            Ex.printStackTrace();
        }

    }
}
