package com.neighboringcountries.rest;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class RestCountriesClient {
    private static IRestCountries iRestCountries;
    private static String baseUrl = "https://restcountries.eu";

    public static IRestCountries getClient()
    {
        if(iRestCountries == null)
        {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    return response;
                }
            });
            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            iRestCountries = client.create(IRestCountries.class);
        }
        return iRestCountries;
    }
}
