package com.neighboringcountries;

import com.neighboringcountries.models.Country;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class DBManager {

    private static DBManager mDBManager;

    public static DBManager getInstance() {

        if (mDBManager == null) {
            mDBManager = new DBManager();
        }
        return mDBManager;
    }



    public boolean saveCountries(List<Country> countries) {

        boolean result = true;
        try {

            for (Country country: countries) {

                try {
                    String borders = StringUtils.join(country.getBorders(), ",");
                    country.setBordersString(borders);
                } catch (Exception ex) {
                    //// TODO: 19/09/2017 - swallowing exception... handle it properly. Meanwhile continue to the next country
                }

                // // TODO: 19/09/2017 - once failed it wont continue to save other contries. Find a better approach
                country.save();
            }
        }
        catch (Exception ex) {
            result = false;
        }

        return result;
    }

    public ArrayList<Country> getAllCountries() {

        String query = "select *  from Country";
        List<Country> countries = Country.findWithQuery(Country.class, query);

        //// TODO: 19/09/2017 handle null pointer or conversion failure
        return (ArrayList<Country>) countries;
    }

    public ArrayList<Country> getNeighboringCountries(Country country) {

        //// TODO: 19/09/2017 handle null pointer or conversion failure
        List<Country> countries = Country.find(Country.class, "instr (?, alphacode) > 0", country.getBordersString());
        return (ArrayList<Country>) countries;
    }


    public boolean isCountriesTblEmpty(){

        List<Country> countries = Country.listAll(Country.class);

        if(countries != null && countries.isEmpty())
            return true;
        else
            return false;
    }

    public void deleteAllCountries(){
        Country.deleteAll(Country.class);
    }
}
