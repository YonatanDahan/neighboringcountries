package com.neighboringcountries.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.neighboringcountries.R;
import com.neighboringcountries.interfaces.MainActivityActionInterface;
import com.neighboringcountries.models.Country;

import java.util.List;


public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    private List<Country> countryList;
    Context context;
    MainActivityActionInterface mainActivityActionInterface;

    public CountryAdapter(List<Country> countryList, Context context) {
        this.countryList = countryList;
        this.context = context;
    }

    public CountryAdapter(List<Country> countryList, Context context, MainActivityActionInterface mainActivityActionInterface) {
        this.countryList = countryList;
        this.context = context;
        this.mainActivityActionInterface = mainActivityActionInterface;
    }

    public class CountryViewHolder extends RecyclerView.ViewHolder
    {
        private TextView lblCbEnglishName, lblCbNativeName;

        public CountryViewHolder(View v) {
            super(v);
            lblCbEnglishName = v.findViewById(R.id.english_name);
            lblCbNativeName = v.findViewById(R.id.native_name);
        }
    }


    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View countryView = LayoutInflater.
                from(context).inflate(R.layout.row_country, parent, false);

        return new CountryViewHolder(countryView);
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, final int position) {
        Country country = countryList.get(position);
        holder.lblCbEnglishName.setText(country.getName());
        holder.lblCbNativeName.setText(country.getNativeName());
        if (mainActivityActionInterface != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainActivityActionInterface.openNeighboringCountries(countryList.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public void setCountryList (List<Country> countryList){
        this.countryList = countryList;
        notifyDataSetChanged();
    }
}
