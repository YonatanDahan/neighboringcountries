package com.neighboringcountries.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class Country extends SugarRecord implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("alpha3Code")
    @Expose
    private String alphacode;

    @SerializedName("borders")
    @Expose
    @Ignore
    private List<String> borders;

    @SerializedName("nativeName")
    @Expose
    private String nativeName;

    @SerializedName("bordersString")
    @Expose
    private String bordersString;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlphacode() {
        return alphacode;
    }

    public void setAlphacode(String alphacode) {
        this.alphacode = alphacode;
    }

    public List<String> getBorders() {
        return borders;
    }

    public void setBorders(List<String> borders) {
        this.borders = borders;
    }

    public String getBordersString() {
        return bordersString;
    }


    public void setBordersString(String bordersString) {
        this.bordersString = bordersString;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }
}
