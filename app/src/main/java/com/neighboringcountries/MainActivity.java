package com.neighboringcountries;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.neighboringcountries.adapters.CountryAdapter;
import com.neighboringcountries.interfaces.MainActivityActionInterface;
import com.neighboringcountries.models.Country;
import com.neighboringcountries.rest.IRestCountries;
import com.neighboringcountries.rest.RestCountriesClient;
import com.orm.SugarContext;

import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity implements MainActivityActionInterface {

    private List<Country> countryList = new ArrayList<>();
    private RecyclerView countryRecyclerViewCb;
    private CountryAdapter countryAdapter;

    //// TODO: 19/09/2017 replace deprecated api
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryRecyclerViewCb = (RecyclerView)findViewById(R.id.list_countries);
        SugarContext.init(this);
        RecyclerView.LayoutManager layoutManagerObj =
                new LinearLayoutManager(getApplicationContext());
        countryRecyclerViewCb.setLayoutManager(layoutManagerObj);
        countryRecyclerViewCb.setItemAnimator(new DefaultItemAnimator());
        countryRecyclerViewCb.addItemDecoration(new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));

        countryAdapter  = new CountryAdapter(countryList, MainActivity.this, this);
        countryRecyclerViewCb.setAdapter(countryAdapter);

        dialog = new ProgressDialog(MainActivity.this);


        if (NeighboringCountriesApplication.getDbManager().isCountriesTblEmpty()) {


            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    dialog.setMessage("טוען נתונים...");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    getCountriesInfo();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                }
            }.execute();
        }
        else{

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    dialog.setMessage("טוען נתונים...");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    countryList = NeighboringCountriesApplication.getDbManager().getAllCountries();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    countryAdapter.setCountryList(countryList);
                    dialog.dismiss();
                }
            }.execute();

        }

    }

    void getCountriesInfo() {
        try {

            IRestCountries iRestCountries = RestCountriesClient.getClient();

            Call<List<Country>> call = iRestCountries.getInfo();

            call.enqueue(new Callback<List<Country>>() {
                @Override
                public void onResponse(Response<List<Country>> response, Retrofit retrofit) {

                    try {
                        List<Country> result = response.body();
                        countryList = result;

                        NeighboringCountriesApplication.getDbManager().saveCountries(countryList);

                        countryAdapter.setCountryList(countryList);
                        dialog.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                        dialog.dismiss();
                        Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    t.getMessage();
                    dialog.dismiss();
                    Toast.makeText(MainActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();

                }
            });
        }
        catch (Exception Ex) {
            Ex.printStackTrace();
            dialog.dismiss();
            Toast.makeText(MainActivity.this,Ex.getMessage(),Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void openNeighboringCountries(Country country) {
        NeighboringCountriesApplication.getNavigationManager().openNeighboringCountries(this,country);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
       if (getSupportFragmentManager().getBackStackEntryCount() == 0){
           setTitle(getString(R.string.neighboring_countries_title));
           countryAdapter.setCountryList(countryList);
       }

    }
}
