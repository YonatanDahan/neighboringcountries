package com.neighboringcountries.rest;

import com.neighboringcountries.models.Country;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;


public interface IRestCountries {

    @GET("/rest/v2/all?fields=name;nativeName;alpha3Code;borders")
    Call<List<Country>> getInfo();
}
