package com.neighboringcountries;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.orm.SugarContext;


public class NeighboringCountriesApplication extends MultiDexApplication {

    private static NavigationManager navigationManager;
    private static DBManager dbManager;

    @Override
    public void onCreate() {
        super.onCreate();

        navigationManager = new NavigationManager();
        SugarContext.init(this);
        dbManager = DBManager.getInstance();
    }

    public static NavigationManager getNavigationManager() {
        return navigationManager;
    }

    public static DBManager getDbManager() {
        return dbManager;
    }

}
