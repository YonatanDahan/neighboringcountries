package com.neighboringcountries.interfaces;

import com.neighboringcountries.models.Country;


public interface MainActivityActionInterface {

    void openNeighboringCountries(Country country);

}
