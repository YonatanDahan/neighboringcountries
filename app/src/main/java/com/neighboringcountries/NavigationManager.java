package com.neighboringcountries;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.neighboringcountries.fragments.FragmentNeighboringCountries;
import com.neighboringcountries.models.Country;


public class NavigationManager {

    public void openNeighboringCountries(AppCompatActivity activity, Country country) {
        Fragment fragmentNeighboringCountries = FragmentNeighboringCountries.newInstance(country);
        activity.getSupportFragmentManager().beginTransaction().addToBackStack(FragmentNeighboringCountries.class.getName()).add(R.id.activity_main, fragmentNeighboringCountries).commit();
    }

}
