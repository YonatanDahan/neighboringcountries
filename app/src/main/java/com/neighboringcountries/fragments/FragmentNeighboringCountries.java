package com.neighboringcountries.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neighboringcountries.NeighboringCountriesApplication;
import com.neighboringcountries.R;
import com.neighboringcountries.adapters.CountryAdapter;
import com.neighboringcountries.models.Country;

import java.util.ArrayList;
import java.util.List;


public class FragmentNeighboringCountries extends Fragment {
    private static final String COUNTRY_KEY = "country_key";
    private Country mCountry;
    View neighboringCountriesView;
    private List<Country> neighboringCountriesList = new ArrayList<>();
    private RecyclerView neighboringCountriesRecyclerViewCb;
    private CountryAdapter neighboringCountriesAdapter;


    public static FragmentNeighboringCountries newInstance(Country country) {
        FragmentNeighboringCountries fragment = new FragmentNeighboringCountries();
        Bundle bundle = new Bundle();
        bundle.putSerializable(COUNTRY_KEY, country);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        mCountry = (Country) getArguments().getSerializable(COUNTRY_KEY);
        getActivity().setTitle(getString(R.string.neighboring_countries_title) + " Of " + mCountry.getName());
        neighboringCountriesView = inflater.inflate(R.layout.fragment_neighboring_countries, container, false);
        neighboringCountriesRecyclerViewCb = neighboringCountriesView.findViewById(R.id.list_neighboring_countries);
        RecyclerView.LayoutManager layoutManagerObj = new LinearLayoutManager(getContext());
        neighboringCountriesRecyclerViewCb.setLayoutManager(layoutManagerObj);
        neighboringCountriesRecyclerViewCb.setItemAnimator(new DefaultItemAnimator());
        neighboringCountriesRecyclerViewCb.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        neighboringCountriesList = NeighboringCountriesApplication.getDbManager().getNeighboringCountries(mCountry);
        if (neighboringCountriesList.size() == 0){
            neighboringCountriesView.findViewById(R.id.list_neighboring_countries).setVisibility(View.GONE);
            neighboringCountriesView.findViewById(R.id.no_borders).setVisibility(View.VISIBLE);
        }
        neighboringCountriesAdapter = new CountryAdapter(neighboringCountriesList, getContext());
        neighboringCountriesRecyclerViewCb.setAdapter(neighboringCountriesAdapter);


        return neighboringCountriesView;
    }
}
