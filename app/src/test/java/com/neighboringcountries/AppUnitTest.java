package com.neighboringcountries;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.neighboringcountries.models.Country;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;



public class AppUnitTest {
    @Test
    public void check_borders_conversion() throws Exception {
        String json = "[{\"name\":\"Israel\",\"alpha3Code\":\"ISR\",\"borders\":[\"EGY\",\"JOR\",\"LBN\",\"SYR\"],\"nativeName\":\"יִשְׂרָאֵל\"}]";
        Type listType = new TypeToken<List<Country>>() {}.getType();
        List<Country> countries = new Gson().fromJson(json, listType);

        String borders = StringUtils.join(countries.get(0).getBorders(), ",");
        countries.get(0).setBordersString(borders);
        assertEquals(countries.get(0).getBordersString(), "EGY,JOR,LBN,SYR");
    }
}
